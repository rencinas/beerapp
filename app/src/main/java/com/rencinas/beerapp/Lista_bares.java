package com.rencinas.beerapp;

import android.app.Fragment;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.rencinas.beerapp.database.Database;
import static com.rencinas.beerapp.database.Constants.*;

public class Lista_bares extends Fragment {

    private Database db;
    private String[] FROM_SHOW = {NAME, SUBJECT };
    private int[] TO = {R.id.studentName, R.id.studentSubject};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_lista_bares, container, false);

        db = new Database(getActivity());
        // Obtiene el cursor con el listado de alumnos de la Base de Datos
        Cursor cursor = db.getStudents();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(), R.layout.student_row, cursor, FROM_SHOW, TO,
                SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        ListView lvStudentsList = (ListView) view.findViewById(R.id.lvListaBares);
        lvStudentsList.setAdapter(adapter);

        return view;
    }
}
