package com.rencinas.beerapp;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;

/**
 * Listener para las tabs de la aplicaci�n
 * Atiende los eventos que se producen cuando se 'navega' por las
 * pesta�as de la aplicaci�n
 *
 * @author Santiago Faci
 * @version curso 2014-2015
 */
public class TabsListener implements ActionBar.TabListener {

    private Fragment fragment;

    public TabsListener(Fragment fragment)
    {
        this.fragment = fragment;
    }

    /**
     * La pesta�a se ha re-seleccionado
     */
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

		// Aqu� por ejemplo se podr�a refrescar la vista
	}

	/**
	 * Se ha seleccionado una pesta�a. Se carga en la pantalla
	 */
	public void onTabSelected(Tab tab, FragmentTransaction ft) {

		ft.replace(R.id.container, fragment);
	}

	/**
	 * Se ha deseleccionado una pesta�a. Se elimina de la pantalla
	 */
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		
		ft.remove(fragment);
	}
}
