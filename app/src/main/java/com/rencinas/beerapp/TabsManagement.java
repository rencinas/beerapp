package com.rencinas.beerapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;

/**
 * Activity principal donde se mostrar� la aplicaci�n y se presentar�
 * la ActionBar con las pesta�as
 *
 * Desde esta Activity se cargan las pesta�as. La funcionalidad de cada una de ellas
 * se har� en su correspondiente fichero de c�digo
 *
 * En esta aplicaci�n se muestra c�mo trabajar con Bases de Datos en Android con SQLite
 * y c�mo presentar la informaci�n utilizando Tabs
 *
 * @author Santiago Faci
 * @version curso 2014-2015
 */
public class TabsManagement extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);

        loadTabs();
    }

    /*
     * Carga las pesta�as para formar el TabHost
     */
    private void loadTabs() {

    	Resources res = getResources();

        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Crea las tabs
        ActionBar.Tab tab1 = actionBar.newTab().setText(res.getString(R.string.action_bares));
        ActionBar.Tab tab2 = actionBar.newTab().setText(res.getString(R.string.action_favoritos));

        // Crea cada Fragment para luego asociarla con la pesta�a que corresponda
        Fragment tabFragment1 = new Lista_bares();
        Fragment tabFragment2 = new lista_bares_favoritos();

        // Asocia cada Fragment con su tab
        tab1.setTabListener(new TabsListener(tabFragment1));
        tab2.setTabListener(new TabsListener(tabFragment2));

        // A�ade las tabs a la ActionBar
        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
    }
}
