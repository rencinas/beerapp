package com.rencinas.beerapp.base;

import android.graphics.Bitmap;

/**
 * Created by Raúl on 19/11/2015.
 */
public class Bar {
    public Bar(){

    }
    private String nombre;
    private String email;
    private Bitmap foto;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Bitmap getFoto() {
        return foto;
    }

    public void setFoto(Bitmap foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
