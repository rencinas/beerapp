package com.rencinas.beerapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class Registro extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action Bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action Bar item clicks here. The action Bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btRegistrar:
                EditText etNombre = (EditText) findViewById(R.id.etNombre);
                EditText etApellidos = (EditText) findViewById(R.id.etApellidos);
                EditText etEmail = (EditText) findViewById(R.id.etEmail);
                ImageButton ibFoto = (ImageButton) findViewById(R.id.ibFotoPerfil);
                break;
            case R.id.btCancelar:

                break;
            default:
                break;
        }
    }
}
